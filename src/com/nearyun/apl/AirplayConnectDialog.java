package com.nearyun.apl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.jmdns.ServiceInfo;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;

import com.nearyun.apl.callback.DialogCallback;
import com.nearyun.apl.service.ServiceInfoEx;
import com.tornado.util.ViewHolderUtil;

public class AirplayConnectDialog extends Dialog implements OnItemClickListener {

	private ConnectListAdapter adapter;

	private DialogCallback callback;

	private String selectedServiceInfoKey;

	private CheckBox deviceSwitch;

	public AirplayConnectDialog(Context context, boolean isSearchSwitchOn, final DialogCallback callback) {
		super(context, R.style.device_custom_dialog);
		this.callback = callback;
		setContentView(R.layout.dialog_airplay_connect_layout);

		adapter = new ConnectListAdapter(getContext(), new ArrayList<ServiceInfoEx>());

		ListView list = (ListView) findViewById(R.id.airplay_listview);
		list.setEmptyView(findViewById(R.id.empty_view));
		list.setAdapter(adapter);
		list.setOnItemClickListener(this);

		deviceSwitch = (CheckBox) findViewById(R.id.device_switch_cb);
		deviceSwitch.setChecked(isSearchSwitchOn);
		deviceSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (!isChecked) {
					adapter.clear();
					adapter.notifyDataSetChanged();
				}
				if (callback != null)
					callback.onSwitchChange(isChecked);
			}
		});

		WifiManager wifi_service = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifi_service.getConnectionInfo();
		TextView curWifiTextView = (TextView) findViewById(R.id.curWifiTextView);
		String tmp = wifiInfo != null ? wifiInfo.getSSID() : "";
		tmp = tmp.contains("<unknown ssid>") ? "未连接" : tmp;
		curWifiTextView.setText(context.getString(R.string.cur_wifi) + tmp);
	}

	public void refreshServiceData(String selectedKey, Collection<ServiceInfoEx> services) {
		selectedServiceInfoKey = selectedKey;

		adapter.clear();
		for (ServiceInfoEx si : services) {
			adapter.add(si);
		}
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		if (callback != null) {
			ServiceInfo si = adapter.getItem(position).getServiceInfo();
			callback.onServiceSelected(si);
		}
		cancel();
	}

	private class ConnectListAdapter extends ArrayAdapter<ServiceInfoEx> {

		private LayoutInflater inflater;

		public ConnectListAdapter(Context context, List<ServiceInfoEx> services) {
			super(context, 0, services);
			inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null)
				convertView = inflater.inflate(R.layout.item_airplay_connect_layout, parent, false);

			ServiceInfoEx si = getItem(position);

			boolean isWorking = TextUtils.equals(selectedServiceInfoKey, si.getKey());

			TextView nameTextview = ViewHolderUtil.get(convertView, R.id.ic_name_textview);
			nameTextview.setText(si.getName());
			if (isWorking) {
				nameTextview.setTextColor(Color.parseColor("#44dd44"));
				nameTextview.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_cast_selected, 0, 0, 0);
			} else {
				nameTextview.setTextColor(Color.parseColor("#444444"));
				nameTextview.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_cast, 0, 0, 0);
			}

			TextView deviceStatusTextView = ViewHolderUtil.get(convertView, R.id.device_status_textview);
			if (TextUtils.isEmpty(si.getServiceInfo().getHostAddress()))
				deviceStatusTextView.setText(R.string.device_offline);
			else
				deviceStatusTextView.setText(R.string.device_online);

			TextView timeTextview = ViewHolderUtil.get(convertView, R.id.time_textview);
			timeTextview.setText(compareTime(si.getUpdateTime(), System.currentTimeMillis()));

			return convertView;
		}

		@Override
		public boolean isEnabled(int position) {
			ServiceInfoEx si = getItem(position);
			return !TextUtils.isEmpty(si.getServiceInfo().getHostAddress());
		}

		private String compareTime(long time, long now) {
			if (now < time)
				return "";
			long diff = (now - time) / 1000;
			if (diff < 10)
				return "刚刚";
			if (diff < 60)
				return diff + "秒前";
			if (diff < 3600) {
				long min = diff / 60;
				return min + "分钟前";
			}
			if (diff < 86400) {
				long hours = diff / 3600;
				return hours + "小时前";
			} else
				return getHourAndMin(time);
		}

		private String getHourAndMin(long time) {
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			return format.format(new Date(time));
		}

	}

}
