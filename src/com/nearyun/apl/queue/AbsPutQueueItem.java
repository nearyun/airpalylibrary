package com.nearyun.apl.queue;

import javax.jmdns.ServiceInfo;

import com.nearyun.apl.callback.AirPlaySendCallback;

public abstract class AbsPutQueueItem extends AbsQueueItem {

	protected String sessionId;
	protected ServiceInfo serviceInfo;
	protected String transition;
	protected AirPlaySendCallback callback;

	public AbsPutQueueItem(String sId, ServiceInfo sInfo, String transition, AirPlaySendCallback cb) {
		this.sessionId = sId;
		this.serviceInfo = sInfo;
		this.transition = transition;
		this.callback = cb;
	}

	public abstract boolean isValidated();

}
