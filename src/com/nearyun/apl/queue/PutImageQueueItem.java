package com.nearyun.apl.queue;

import java.io.BufferedOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.jmdns.ServiceInfo;

import android.text.TextUtils;

import com.nearyun.apl.callback.AirPlaySendCallback;
import com.tornado.util.EncryptionUtil;

public class PutImageQueueItem extends AbsPutQueueItem {
	// private static final int BUFFER_LENGTH = 32768;// 32 * 1024
	private byte[] source;

	public PutImageQueueItem(byte[] src, String sId, ServiceInfo sInfo, String transition, AirPlaySendCallback cb) {
		super(sId, sInfo, transition, cb);
		this.source = src;
	}

	@Override
	public boolean isValidated() {
		return source != null && source.length != 0 && !TextUtils.isEmpty(sessionId) && serviceInfo != null;
	}

	@Override
	public void execute() {
		try {
			@SuppressWarnings("deprecation")
			URL url = new URL(serviceInfo.getURL() + "/photo");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000);
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Content-Length", "" + source.length);
			conn.setRequestProperty("X-Apple-AssetKey", EncryptionUtil.MD5(System.currentTimeMillis() + ""));
			conn.setRequestProperty("X-Apple-Session-ID", sessionId);
			conn.setRequestProperty("X-Apple-Transition", transition);
			conn.setRequestProperty("User-Agent", "MediaControl/1.0");

			BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
			// BufferedInputStream in = new BufferedInputStream(new
			// ByteArrayInputStream(source));
			// byte[] buffer = new byte[BUFFER_LENGTH];
			// int i;
			// while ((i = in.read(buffer)) != -1) {
			// out.write(buffer, 0, i);
			// }
			// in.close();
			out.write(source);
			out.close();

			int status = conn.getResponseCode();
			if (status == 200) {
				if (callback != null)
					callback.onSuccess();
			} else {
				if (callback != null)
					callback.onFailure(status, conn.getResponseMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (callback != null)
				callback.onError(e);
		}
	}

}
