package com.nearyun.apl.queue;

public abstract class AbsQueueItem {

	public abstract void execute();
}
