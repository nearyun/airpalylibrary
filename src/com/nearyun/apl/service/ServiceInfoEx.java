package com.nearyun.apl.service;

import javax.jmdns.ServiceInfo;

public class ServiceInfoEx {

	private ServiceInfo mServiceInfo;

	private long updateTime;

	public ServiceInfoEx(ServiceInfo si, long time) {
		this.mServiceInfo = si;
		this.updateTime = time;
	}

	public String getKey() {
		return mServiceInfo.getKey();
	}

	public String getName() {
		return mServiceInfo.getName();
	}

	public ServiceInfo getServiceInfo() {
		return mServiceInfo;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void updateTime(long time) {
		updateTime = time;
	}
}
