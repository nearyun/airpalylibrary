package com.nearyun.apl.service;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.Handler;
import android.text.TextUtils;

import com.nearyun.apl.AirplayConnectDialog;
import com.nearyun.apl.callback.AirPlaySendCallback;
import com.nearyun.apl.callback.DialogCallback;
import com.nearyun.apl.queue.PutImageQueueItem;
import com.nearyun.apl.runnable.StopDisplayRunnable;
import com.nearyun.apl.util.NetworkUtil;
import com.tornado.util.XHLogUtil;

public class AndroidAirPlay {
	private final String TAG = AndroidAirPlay.class.getSimpleName();

	private final String FILE_NAME = "AndroidAirPlay.io";
	private final String KEY_SELECTED_SERVICE = "selectedService";

	// the service type (which events to listen for)
	private static final String SERVICE_TYPE = "_airplay._tcp.local.";

	private final ArrayList<OnDeviceChangedListener> listenerArray = new ArrayList<OnDeviceChangedListener>();

	public void addOnServiceInfoChangedListener(OnDeviceChangedListener listener) {
		if (listener != null)
			listenerArray.add(listener);
	}

	public void removeDeviceListener(OnDeviceChangedListener listener) {
		listenerArray.remove(listener);
	}

	public interface OnDeviceChangedListener {

		public void onDeviceRemoved();

		public void onDeviceResolved();

		public void onDeviceLoss();
	}

	// app preferences
	private SharedPreferences prefs;

	// JmDNS library
	private JmDNS jmdns;

	// the lock to enable discovery (due to saving battery)
	private MulticastLock lock;

	// holder for the currently selected service
	private String selectedService = null;

	// map of services discovered (continuously updated in background)
	private final Map<String, ServiceInfoEx> servicesHashMap = new HashMap<String, ServiceInfoEx>();

	private Context mContext;

	private Handler mHandler = new Handler();

	private AirplayQueue mAirplayQueue;

	private boolean isSearchSwitchOn = false;

	private ServiceListener mServiceListener = new ServiceListener() {

		@Override
		public void serviceAdded(final ServiceEvent event) {
			XHLogUtil.i(TAG, "Found AirPlay service getName: " + event.getInfo().getName());

			new Thread() {
				@Override
				public void run() {
					if (isDNSReady())
						jmdns.requestServiceInfo(event.getType(), event.getName(), 1000);
					else
						XHLogUtil.e(TAG, "Error: jmdns is null!");
				}
			}.start();
		}

		@Override
		public void serviceRemoved(ServiceEvent event) {
			XHLogUtil.i(TAG, "Removed AirPlay service: " + event.getName());
			servicesHashMap.remove(event.getInfo().getKey());
			if (selectedService != null && TextUtils.equals(selectedService, event.getName())) {
				selectedService = null;
			}

			mHandler.removeCallbacks(refreshServiceDataRunnable);
			mHandler.postDelayed(refreshServiceDataRunnable, 100);

			for (int i = 0; i < listenerArray.size(); i++) {
				listenerArray.get(i).onDeviceRemoved();
			}
		}

		@Override
		public void serviceResolved(ServiceEvent event) {
			XHLogUtil.i(TAG, "Resolved AirPlay service getInfo: " + event.getName());
			String key = event.getInfo().getKey();
			if (servicesHashMap.containsKey(key)) {
				ServiceInfoEx siex = servicesHashMap.get(key);
				if (siex != null)
					siex.updateTime(System.currentTimeMillis());
				else
					servicesHashMap.put(key, new ServiceInfoEx(event.getInfo(), System.currentTimeMillis()));
			} else {
				servicesHashMap.put(key, new ServiceInfoEx(event.getInfo(), System.currentTimeMillis()));
			}

			mHandler.removeCallbacks(refreshServiceDataRunnable);
			mHandler.postDelayed(refreshServiceDataRunnable, 100);

			if (selectedService == null) {
				String remembered = prefs.getString(KEY_SELECTED_SERVICE, null);
				if (remembered != null && remembered.equals(event.getInfo().getKey())) {
					selectedService = remembered;
				}
			}

			for (int i = 0; i < listenerArray.size(); i++) {
				listenerArray.get(i).onDeviceResolved();
			}
		}

	};

	private Runnable refreshServiceDataRunnable = new Runnable() {
		@Override
		public void run() {
			if (mAirplayConnectDialog != null && mAirplayConnectDialog.isShowing())
				mAirplayConnectDialog.refreshServiceData(selectedService, getServiceInfos());
		}
	};

	private int errorCount = 0;
	private AirPlaySendCallback mAirPlaySendCallback = new AirPlaySendCallback() {

		@Override
		public void onSuccess() {
			XHLogUtil.d(TAG, "airplay send onSuccess");
			errorCount = 0;
		}

		@Override
		public void onFailure(int code, String reason) {
			XHLogUtil.w(TAG, "airplay send onFailure " + code);
			if (code == 412) {
				errorCount++;
				if (errorCount > 10) {
					resetSessionId();
				}
			}
		}

		@Override
		public void onError(Exception e) {
			XHLogUtil.e(TAG, "airplay send onError");
			stopQueue();
			for (int i = 0; i < listenerArray.size(); i++) {
				listenerArray.get(i).onDeviceLoss();
			}
		}

	};

	private static AndroidAirPlay mAndroidAirPlay;

	public static AndroidAirPlay getInstance() {
		if (mAndroidAirPlay == null) {
			mAndroidAirPlay = new AndroidAirPlay();
		}
		return mAndroidAirPlay;
	}

	private AndroidAirPlay() {
		mAirplayQueue = new AirplayQueue();

	}

	public void refresh() {
		if (isDNSReady()) {
			jmdns.removeServiceListener(SERVICE_TYPE, mServiceListener);
			jmdns.addServiceListener(SERVICE_TYPE, mServiceListener);
			isSearchSwitchOn = true;
		} else
			XHLogUtil.w(TAG, "warn:jmdns not ready, can not refresh");
	}

	private final Object JM_DNS_LOCKER = new Object();

	public void init(Context context) {
		mContext = context;
		prefs = mContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

		// acquire multicast lock
		WifiManager wifi = (WifiManager) mContext.getSystemService(android.content.Context.WIFI_SERVICE);
		lock = wifi.createMulticastLock("JmDNSLock");
		lock.setReferenceCounted(true);
		lock.acquire();

	}

	public void startJmDnsSearch() {
		new Thread() {
			@Override
			public void run() {
				startJmDNS();
			}
		}.start();
	}

	private void startJmDNS() {
		synchronized (JM_DNS_LOCKER) {
			if (isDNSReady()) {
				XHLogUtil.w(TAG, "warn:dns is running");
				return;
			}
			InetAddress deviceAddress = NetworkUtil.getWifiInetAddress(mContext);
			if (deviceAddress == null) {
				XHLogUtil.e(TAG, "Error: Unable to get local IP address");
				return;
			}

			try {
				XHLogUtil.i(TAG, "execute start jmDNS");
				jmdns = JmDNS.create(deviceAddress);
				XHLogUtil.e(TAG, "Using local address " + deviceAddress.getHostAddress());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void stopJmDNS() {
		synchronized (JM_DNS_LOCKER) {
			if (!isDNSReady())
				return;
			try {
				XHLogUtil.i(TAG, "execute stop jmDNS");
				jmdns.removeServiceListener(SERVICE_TYPE, mServiceListener);
				isSearchSwitchOn = false;
				jmdns.close();
				XHLogUtil.i(TAG, "stop jmDNS end");
			} catch (Exception e) {
				XHLogUtil.e(TAG, "Error: " + e.getMessage());
			} finally {
				jmdns = null;
			}
		}
	}

	public void stopJmDNSSearch() {
		new Thread() {
			@Override
			public void run() {
				stopJmDNS();
			}
		}.start();
	}

	public boolean isDNSReady() {
		return jmdns != null;
	}

	private void startQueue() {
		stopQueue();
		new Thread(mAirplayQueue).start();
	}

	private void stopQueue() {
		if (mAirplayQueue.isRunning())
			mAirplayQueue.stop();
	}

	public void clearQueue() {
		if (mAirplayQueue.isRunning())
			mAirplayQueue.clearQueue();
	}

	public void release() {
		// if (selectedService != null) {
		// prefs.edit().putString(KEY_SELECTED_SERVICE,
		// selectedService).apply();
		// selectedService = null;
		// }

		dismissServiceListDialog();

		stopAirplayDisplay();

		servicesHashMap.clear();
		listenerArray.clear();

		stopJmDNSSearch();

		if (lock != null)
			lock.release();

		if (mAirplayQueue != null)
			mAirplayQueue.stop();

		mAndroidAirPlay = null;
	}

	private AirplayConnectDialog mAirplayConnectDialog = null;

	public void dismissServiceListDialog() {
		if (mAirplayConnectDialog != null && mAirplayConnectDialog.isShowing())
			mAirplayConnectDialog.dismiss();
	}

	public void showServiceList(Context context, final DialogCallback callback) {
		if (mAirplayConnectDialog != null && mAirplayConnectDialog.isShowing())
			mAirplayConnectDialog.dismiss();
		mAirplayConnectDialog = new AirplayConnectDialog(context, isSearchSwitchOn, new DialogCallback() {

			@Override
			public void onServiceSelected(ServiceInfo serviceInfo) {
				selectedService = serviceInfo.getKey();
				startQueue();
				if (callback != null)
					callback.onServiceSelected(serviceInfo);
			}

			@Override
			public void onSwitchChange(boolean flag) {
				if (flag) {
					refresh();
				} else {
					stopQueue();
					stopAirplayDisplay();
					selectedService = null;
					isSearchSwitchOn = false;
					servicesHashMap.clear();
					if (callback != null)
						callback.onSwitchChange(flag);
				}

			}

		});
		mAirplayConnectDialog.refreshServiceData(selectedService, getServiceInfos());
		mAirplayConnectDialog.show();
	}

	protected Collection<ServiceInfoEx> getServiceInfos() {
		return servicesHashMap.values();
	}

	private void stopAirplayDisplay() {
		if (TextUtils.isEmpty(selectedService)) {
			return;
		}
		XHLogUtil.d(TAG, "stopAirplayDisplay");
		ServiceInfoEx siex = servicesHashMap.get(selectedService);
		if (siex != null) {
			new Thread(new StopDisplayRunnable(sessionId, siex.getServiceInfo(), null)).start();
		}
	}

	public boolean isWorking() {
		return mAirplayQueue.isRunning();
	}

	private final String TRANSITION_NONE = "None";
	private String sessionId = UUID.randomUUID().toString();

	private void resetSessionId() {
		sessionId = UUID.randomUUID().toString();
	}

	public void putImage(byte[] source) {
		if (source == null || source.length == 0) {
			XHLogUtil.e(TAG, "source not be null or empty");
			return;
		}

		if (!mAirplayQueue.isRunning()) {
			XHLogUtil.w(TAG, "queue is not running");
			return;
		}

		if (TextUtils.isEmpty(selectedService)) {
			XHLogUtil.e(TAG, "device has not been selected");
			return;
		}
		ServiceInfoEx siex = servicesHashMap.get(selectedService);
		if (siex != null) {
			XHLogUtil.d(TAG, "add source to queue : " + source.length);
			mAirplayQueue.add(new PutImageQueueItem(source, sessionId, siex.getServiceInfo(), TRANSITION_NONE,
					mAirPlaySendCallback));
		}
	}

}
