package com.nearyun.apl.service;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.nearyun.apl.queue.AbsQueueItem;
import com.nearyun.apl.queue.NullQueueItem;
import com.nearyun.apl.queue.PutImageQueueItem;
import com.tornado.util.XHLogUtil;

public class AirplayQueue implements Runnable {
	private String TAG = AirplayQueue.class.getSimpleName();

	private final int maxSize = 5;

	private ArrayBlockingQueue<AbsQueueItem> mArrayBlockingQueue = new ArrayBlockingQueue<AbsQueueItem>(maxSize);

	private boolean isRunning = false;

	private AbsQueueItem lastQueueItem = null;

	public boolean isRunning() {
		return isRunning;
	}

	@Override
	public void run() {
		if (isRunning)
			return;

		isRunning = true;
		lastQueueItem = null;

		AbsQueueItem item = null;

		while (isRunning) {
			try {
				item = mArrayBlockingQueue.poll(10, TimeUnit.SECONDS);
				if (item == null) {
					if (lastQueueItem != null && isRunning) {
						lastQueueItem.execute();
					} else {
						XHLogUtil.w(TAG, "no data");
					}
					continue;
				}

				if (item instanceof NullQueueItem)
					break;

				lastQueueItem = item;
				if (isRunning) {
					lastQueueItem.execute();
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		XHLogUtil.d(TAG, "===> queue is terminal");
		clearQueue();
	}

	public int getQueueSize() {
		return mArrayBlockingQueue.size();
	}

	public void clearQueue() {
		mArrayBlockingQueue.clear();
	}

	public void stop() {
		if (!isRunning)
			return;
		isRunning = false;
		clearQueue();
		try {
			mArrayBlockingQueue.put(new NullQueueItem());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void add(PutImageQueueItem src) {
		if (src == null || !src.isValidated() || !isRunning)
			return;
		try {
			mArrayBlockingQueue.add(src);
		} catch (IllegalStateException ise) {
			XHLogUtil.w(TAG, "queue is full");
			XHLogUtil.printStackTrace(ise);
			mArrayBlockingQueue.poll();
			add(src);
		} catch (Exception e) {
			XHLogUtil.printStackTrace(e);
		}
	}
}
