//package com.nearyun.apl;
//
//import java.io.File;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//import javax.jmdns.ServiceInfo;
//
//import android.util.Log;
//
//import com.nearyun.apl.callback.AirPlayHttpClientCallback;
//import com.nearyun.apl.runnable.PutImageCacheRunnable;
//import com.nearyun.apl.runnable.PutImageDisplayRunnable;
//import com.nearyun.apl.runnable.PutImageFromByteArrayRunnable;
//import com.nearyun.apl.runnable.PutImageRunnable;
//import com.nearyun.apl.runnable.StopDisplayRunnable;
//
///**
// * Class used to communicate to the AirPlay-enabled device.
// * 
// * @author Tuomas Tikka
// */
//public class AirPlayHttpClient {
//	private final String TAG = AirPlayHttpClient.class.getSimpleName();
//
//	private final String TRANSITION_NONE = "None";
//	@SuppressWarnings("unused")
//	private final String TRANSITION_RANDOM = "Random";
//	@SuppressWarnings("unused")
//	private final String TRANSITION_DISSOLVE = "Dissolve";
//	@SuppressWarnings("unused")
//	private final String TRANSITION_SLIDELEFT = "SlideLeft";
//	@SuppressWarnings("unused")
//	private final String TRANSITION_SLIDERIGHT = "SlideRight";
//
//	private ExecutorService executorService = Executors.newSingleThreadExecutor();
//
//	public void shutdown() {
//		if (!executorService.isShutdown()) {
//			executorService.shutdown();
//		}
//	}
//
//	public boolean stopDisplay(String sessionId, ServiceInfo serviceInfo, AirPlayHttpClientCallback callback) {
//		if (serviceInfo == null) {
//			Log.w(TAG, "Not connected to AirPlay service");
//			return false;
//		} else {
//			executorService.submit(new StopDisplayRunnable(sessionId, serviceInfo, callback));
//			return true;
//		}
//
//	}
//
//	protected boolean putImage(String sessionId, byte[] source, ServiceInfo serviceInfo,
//			AirPlayHttpClientCallback callback) {
//		if (serviceInfo == null) {
//			Log.w(TAG, "Not connected to AirPlay service");
//			return false;
//		} else {
//			executorService.submit(new PutImageFromByteArrayRunnable(source, sessionId, serviceInfo, TRANSITION_NONE,
//					callback));
//			return true;
//		}
//	}
//
//	protected boolean putImage(String sessionId, File file, ServiceInfo serviceInfo, AirPlayHttpClientCallback callback) {
//		if (serviceInfo == null) {
//			Log.w(TAG, "Not connected to AirPlay service");
//			return false;
//		} else {
//			executorService.submit(new PutImageRunnable(file, sessionId, serviceInfo, TRANSITION_NONE, callback));
//			return true;
//		}
//	}
//
//	protected boolean putImageForCache(String sessionId, File file, ServiceInfo serviceInfo,
//			AirPlayHttpClientCallback callback) {
//		if (serviceInfo == null) {
//			Log.w(TAG, "Not connected to AirPlay service");
//			return false;
//		} else {
//			executorService.submit(new PutImageCacheRunnable(file, sessionId, serviceInfo, TRANSITION_NONE, callback));
//			return true;
//		}
//	}
//
//	protected boolean putImageForDisplay(String sessionId, File file, ServiceInfo serviceInfo,
//			AirPlayHttpClientCallback callback) {
//		if (serviceInfo == null) {
//			Log.w(TAG, "Not connected to AirPlay service");
//			return false;
//		} else {
//			executorService
//					.submit(new PutImageDisplayRunnable(file, sessionId, serviceInfo, TRANSITION_NONE, callback));
//			return true;
//		}
//	}
//
//}
