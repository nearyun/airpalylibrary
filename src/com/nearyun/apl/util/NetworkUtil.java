package com.nearyun.apl.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Locale;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {
	private static final int TYPE_NONE = -1;
	private static final int TYPE_WIFI = 1;
	private static final int TYPE_CMWAP = 2;
	private static final int TYPE_CMNET = 3;

	/**
	 * 获取当前的网络状态 -1：没有网络 1：WIFI网络 2：wap网络 3：net网络
	 * 
	 * @param context
	 * @return
	 */
	private static int getAPNType(Context context) {
		int netType = TYPE_NONE;
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo == null) {
			return netType;
		}
		int nType = networkInfo.getType();
		if (nType == ConnectivityManager.TYPE_MOBILE) {
			if (networkInfo.getExtraInfo().toLowerCase(Locale.getDefault()).equals("cmnet")) {
				netType = TYPE_CMNET;
			} else {
				netType = TYPE_CMWAP;
			}
		} else if (nType == ConnectivityManager.TYPE_WIFI) {
			netType = TYPE_WIFI;
		}
		return netType;
	}

	private static boolean isWIFI(Context context) {
		return getAPNType(context) == TYPE_WIFI;
	}

	public static InetAddress getWifiInetAddress(Context context) {
		return isWIFI(context) ? getInetAddress() : null;
	}

	private static InetAddress getInetAddress() {
		try {
			Enumeration<NetworkInterface> interfacesEnumList = NetworkInterface.getNetworkInterfaces();
			if (interfacesEnumList == null)
				return null;
			while (interfacesEnumList.hasMoreElements()) {
				NetworkInterface interfaceModel = interfacesEnumList.nextElement();
				Enumeration<InetAddress> enumIpAddr = interfaceModel.getInetAddresses();
				while (enumIpAddr.hasMoreElements()) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
						return inetAddress;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
