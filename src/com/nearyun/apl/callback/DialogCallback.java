package com.nearyun.apl.callback;

import javax.jmdns.ServiceInfo;

public interface DialogCallback {

	public void onServiceSelected(ServiceInfo serviceInfo);

	public void onSwitchChange(boolean flag);
}
