package com.nearyun.apl.callback;

public interface AirPlaySendCallback extends AirPlayHttpClientCallback {

	public void onError(Exception e);
}
