package com.nearyun.apl.callback;

/**
 * Interface used as a callback to deliver notifications from AirPlay client
 * service back to UI.
 * 
 */
public interface AirPlayHttpClientCallback {

	public void onSuccess();

	public void onFailure(int code, String reason);

}
