//package com.nearyun.apl.runnable;
//
//import java.io.File;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
//import javax.jmdns.ServiceInfo;
//
//import com.nearyun.apl.callback.AirPlayHttpClientCallback;
//import com.nearyun.apl.util.EncryptionUtil;
//
//public class PutImageDisplayRunnable extends AbsPutRunnable {
//	private File file;
//
//	public PutImageDisplayRunnable(File file, String sessionId, ServiceInfo serviceInfo, String transition,
//			AirPlayHttpClientCallback callback) {
//		super(sessionId, serviceInfo, transition, callback);
//		this.file = file;
//	}
//
//	@Override
//	public void run() {
//		try {
//			@SuppressWarnings("deprecation")
//			URL url = new URL(serviceInfo.getURL() + "/photo");
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setDoInput(true);
//			conn.setDoOutput(true);
//			conn.setConnectTimeout(10 * 1000);
//			conn.setReadTimeout(10 * 1000);
//			conn.setRequestMethod("PUT");
//			conn.setRequestProperty("Content-Length", "0");
//			conn.setRequestProperty("User-Agent", "MediaControl/1.0");
//			conn.setRequestProperty("X-Apple-AssetKey", EncryptionUtil.MD5(file.toString()));
//			conn.setRequestProperty("X-Apple-Session-ID", sessionId);
//			conn.setRequestProperty("X-Apple-AssetAction", "displayCached");
//			conn.setRequestProperty("X-Apple-Transition", transition);
//
//			int status = conn.getResponseCode();
//			if (status == 200) {
//				if (callback != null)
//					callback.onSuccess();
//			} else {
//				if (callback != null)
//					callback.onFailure(status, conn.getResponseMessage());
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			if (callback != null)
//				callback.onFailure(-1, e.getMessage() + "");
//		}
//	}
//}
