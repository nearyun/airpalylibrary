package com.nearyun.apl.runnable;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.jmdns.ServiceInfo;

import com.nearyun.apl.callback.AirPlaySendCallback;

public class StopDisplayRunnable implements Runnable {

	private String sessionId;
	private ServiceInfo serviceInfo;
	private AirPlaySendCallback callback;

	public StopDisplayRunnable(String sid, ServiceInfo sinfo, AirPlaySendCallback cb) {
		sessionId = sid;
		serviceInfo = sinfo;
		callback = cb;
	}

	@Override
	public void run() {
		try {
			@SuppressWarnings("deprecation")
			URL url = new URL(serviceInfo.getURL() + "/stop");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", "0");
			conn.setRequestProperty("X-Apple-Session-ID", sessionId);
			conn.setRequestProperty("User-Agent", "MediaControl/1.0");

			int status = conn.getResponseCode();
			if (status == 200) {
				if (callback != null)
					callback.onSuccess();
			} else {
				if (callback != null)
					callback.onFailure(status, conn.getResponseMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (callback != null)
				callback.onError(e);
		}
	}

}
