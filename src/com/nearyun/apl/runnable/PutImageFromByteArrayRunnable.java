//package com.nearyun.apl.runnable;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.ByteArrayInputStream;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.Arrays;
//
//import javax.jmdns.ServiceInfo;
//
//import com.nearyun.apl.callback.AirPlayHttpClientCallback;
//import com.nearyun.apl.util.EncryptionUtil;
//
//public class PutImageFromByteArrayRunnable extends AbsPutRunnable {
//	private byte[] source;
//
//	public PutImageFromByteArrayRunnable(byte[] src, String sessionId, ServiceInfo serviceInfo, String transition,
//			AirPlayHttpClientCallback callback) {
//		super(sessionId, serviceInfo, transition, callback);
//		source = Arrays.copyOf(src, src.length);
//	}
//
//	@Override
//	public void run() {
//		try {
//			@SuppressWarnings("deprecation")
//			URL url = new URL(serviceInfo.getURL() + "/photo");
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setDoInput(true);
//			conn.setDoOutput(true);
//			conn.setConnectTimeout(10 * 1000);
//			conn.setReadTimeout(10 * 1000);
//			conn.setRequestMethod("PUT");
//			conn.setRequestProperty("Content-Length", "" + source.length);
//			conn.setRequestProperty("X-Apple-AssetKey", EncryptionUtil.MD5(System.currentTimeMillis() + ""));
//			conn.setRequestProperty("X-Apple-Session-ID", sessionId);
//			conn.setRequestProperty("X-Apple-Transition", transition);
//			conn.setRequestProperty("User-Agent", "MediaControl/1.0");
//
//			BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
//			BufferedInputStream in = new BufferedInputStream(new ByteArrayInputStream(source));
//			byte[] buffer = new byte[32 * 1024];
//			int i;
//			while ((i = in.read(buffer)) != -1) {
//				out.write(buffer, 0, i);
//			}
//			in.close();
//			out.close();
//
//			int status = conn.getResponseCode();
//			if (status == 200) {
//				if (callback != null)
//					callback.onSuccess();
//			} else {
//				if (callback != null)
//					callback.onFailure(status, conn.getResponseMessage());
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			if (callback != null)
//				callback.onFailure(-1, e.getMessage() + "");
//		}
//	}
//
//}
