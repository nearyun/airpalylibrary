//package com.nearyun.apl.runnable;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
//import javax.jmdns.ServiceInfo;
//
//import com.nearyun.apl.callback.AirPlayHttpClientCallback;
//import com.nearyun.apl.util.EncryptionUtil;
//
//public class PutImageRunnable extends AbsPutRunnable {
//	private File file;
//
//	public PutImageRunnable(File file, String sessionId, ServiceInfo serviceInfo, String transition,
//			AirPlayHttpClientCallback callback) {
//		super(sessionId, serviceInfo, transition, callback);
//		this.file = file;
//	}
//
//	@Override
//	public void run() {
//		try {
//			@SuppressWarnings("deprecation")
//			URL url = new URL(serviceInfo.getURL() + "/photo");
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setDoInput(true);
//			conn.setDoOutput(true);
//			conn.setConnectTimeout(10 * 1000);
//			conn.setReadTimeout(10 * 1000);
//			conn.setRequestMethod("PUT");
//			conn.setRequestProperty("Content-Length", "" + file.length());
//			conn.setRequestProperty("X-Apple-AssetKey", EncryptionUtil.MD5(file.toString()));
//			conn.setRequestProperty("X-Apple-Session-ID", sessionId);
//			conn.setRequestProperty("X-Apple-Transition", transition);
//			conn.setRequestProperty("User-Agent", "MediaControl/1.0");
//
//			BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
//			BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
//			byte[] buffer = new byte[32 * 1024];
//			int i;
//			while ((i = in.read(buffer)) != -1) {
//				out.write(buffer, 0, i);
//			}
//			in.close();
//			out.close();
//
//			int status = conn.getResponseCode();
//			if (status == 200) {
//				if (callback != null)
//					callback.onSuccess();
//			} else {
//				if (callback != null)
//					callback.onFailure(status, conn.getResponseMessage());
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			if (callback != null)
//				callback.onFailure(-1, e.getMessage() + "");
//		}
//	}
//
//}
